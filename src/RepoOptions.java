/**
 * File: src/RepoOptions.java
 * -------------------------------------------------------------------------------------------
 * Date			Author      Changes
 * -------------------------------------------------------------------------------------------
 * 03/31/14		hcai		created; for command-line argument handling of the RepoMiner tool
 * 04/03/14		hcai		more options, for buffering converted xml files into separate place and providing flexible ways to input revisions considered
 *
*/
import java.util.ArrayList;
import java.util.List;

public class RepoOptions {
	/** example inputs */
	/*
	String url = "http://svn.apache.org/repos/asf/pdfbox/trunk/";
	String name = "null";
	String password = "null";
	String repoPath = "./testRepo/";
	long start = 924515;
	long end = 924520;
	*/
	protected String url = "";
	protected String name = "";
	protected String password = "";
	protected String repoPath = "";
	protected String fnRevList = "";
	protected String fnRevPairs = "";
	protected String converterPath = ""; // by default we use src2srcml as the converter, so this is the path of the src2srcml binary
	protected boolean checkout = false;
	protected boolean outputDiff = false; // by default, results will simply be dumped to stdout; otherwise, a file will be created, per revision pair, to include the differences
	protected long startRev = -1;
	protected long endRev = -1;
	protected long nRev = 2;
	
	public String url() { return url; }
	public String name() { return name; }
	public String password() { return password; }
	public String repoPath() { return repoPath; }
	public String fnRevList() { return fnRevList; }
	public String fnRevPairs() { return fnRevPairs; }
	public String converterPath() { return converterPath; }
	public boolean checkout() { return checkout; }
	public boolean outputDiff() { return outputDiff; }
	public long startRev() { return startRev; }
	public long endRev() { return endRev; }
	public long nRev() { return nRev; }	
	
	public String[] process(String[] args) throws Exception {
		List<String> argsFiltered = new ArrayList<String>();
		
		for (int i = 0; i < args.length; ++i) {
			String arg = args[i];

			try {
				if (arg.equals("-url")) {
					url	= args[i+1];
					i++;
				}
				else if (arg.equals("-name")) {
					name = args[i+1];
					i++;
				}
				else if (arg.equals("-password")) {
					password = args[i+1];
					i++;
				}
				else if (arg.equals("-repoPath")) {
					repoPath = args[i+1];
					i++;
				}
				else if (arg.equals("-startRev")) {
					startRev = Long.parseLong(args[i+1]);
					i++;
				}
				else if (arg.equals("-endRev")) {
					endRev = Long.parseLong(args[i+1]);
					i++;
				}
				else if (arg.equals("-nRev")) {
					nRev = Long.parseLong(args[i+1]);
					i++;
				}
				else if (arg.equals("-fnRevList")) {
					fnRevList = args[i+1];
					i++;
				}
				else if (arg.equals("-fnRevPairs")) {
					fnRevPairs = args[i+1];
					i++;
				}
				else if (arg.equals("-fnConverter")) {
					converterPath = args[i+1];
					i++;
				}
				else if (arg.equals("-checkout")) {
					checkout = true;
				}
				else if (arg.equals("-outputDiff")) {
					outputDiff = true;
				}
				else {
					argsFiltered.add(arg);
				}
			}
			catch (Exception e) {
				throw e;
			}
		}
		
		String[] arrArgsFilt = new String[argsFiltered.size()];
		return (String[]) argsFiltered.toArray(arrArgsFilt);
	}
}

/* vim :set ts=4 tw=4 tws=4 */

