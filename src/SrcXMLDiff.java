/**
 * File: src/SrcXMLDiff.java
 * -------------------------------------------------------------------------------------------
 * Date			Author      Changes
 * -------------------------------------------------------------------------------------------
 * 04/02/14		hcai		created; differencing two srcXML files to produce method-level differences
 * 04/04/14		hcai		automatically retrieve full-qualified names for class, method, return type and parameter types; 
 * 							reached the fully working version
 * 04/05/14		hcai		accommodated to the srcXML bug (which produces mismatched beginning <class> node for Javadoc.java in Ant-1.0.4 thus failure in locating 
 * 							the parent <class> node of a function inside the solitary anonymous inner class of the Javadoc class), by using the top-level class as the parent 
 * 04/06/14		hcai		removed parameter names, and access specifiers for method names, from method signature compositions
 * 04/09/14		hcai		made full-qualified names of return types more accurate
 * 04/10/14		hcai		corrected retrieving full-qualified name for generic types such as Collection<Filter, Object>   
*/
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.io.File;
import java.util.Map;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;

//import org.reflections.*;

public class SrcXMLDiff {
	public static final List<String> allChangedMethods = new ArrayList<String>();
	public static final List<String> allAddedMethods = new ArrayList<String>();
	public static final List<String> allDeletedMethods = new ArrayList<String>();
	
	public File firstFile;
	public Document firstDoc;
	public String firstPackageName="";
	// map from lean type name to full-namespace-prefixed type name included in the imports
	public Map<String, String> firstImportedTypes = new LinkedHashMap<String, String>();
	
	public File nextFile;
	public Document nextDoc;
	public String nextPackageName="";
	// map from lean type name to full-namespace-prefixed type name included in the imports
	public Map<String, String> nextImportedTypes = new LinkedHashMap<String, String>();
	
	// map from lean type name to full-qualified type name not included in the imports but still resolvable by JVM (Java legacy libraries, say)
	public Map<String, String> legacyTypes = new LinkedHashMap<String, String>();
	
	/*
	public void initLegacyTypes() {
		Reflections refn = new Reflections();
		Set<Class<?>> allClasses = refn.getSubTypesOf(Object.class);
		for (Class<?> cls : allClasses) {
			legacyTypes.put(cls.getSimpleName(), cls.getCanonicalName());
		}
	}
	*/
	
	public SrcXMLDiff(File firstFile, File nextFile) {
		try {
			this.firstFile = firstFile;
			this.nextFile = nextFile;
			
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			this.firstDoc = dBuilder.parse(firstFile);	
			this.nextDoc = dBuilder.parse(nextFile);
			
			firstDoc.normalize();
			nextDoc.normalize();
			
			firstPackageName = retrievePackageAndImports(firstDoc, firstImportedTypes);
			nextPackageName = retrievePackageAndImports(nextDoc, nextImportedTypes);
			
			//initLegacyTypes();			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	
	}
	private static String retrievePackageAndImports(Document doc, Map<String, String> importedTypes) {
		String packname = "";
		NodeList packnode = doc.getElementsByTagName("package");
		if (packnode.getLength() >= 1) {
			assert packnode.getLength() == 1; // a Java source code file should have at most one package declaration statement
			Node sibling = packnode.item(0).getFirstChild();
			assert !(sibling instanceof Element) && sibling.getTextContent().trim().compareTo("package")==0;
			while (sibling != null && !(sibling instanceof Element)) {
				sibling = sibling.getNextSibling();
			}
			packname = sibling.getTextContent();
		}
		
		NodeList impnode = doc.getElementsByTagName("import");
		for (int i = 0; i < impnode.getLength(); i++) {
			Node sibling = impnode.item(i).getFirstChild();
			assert !(sibling instanceof Element) && sibling.getTextContent().trim().startsWith("import");
			while (sibling != null && !(sibling instanceof Element)) {
				sibling = sibling.getNextSibling();
			}
			importedTypes.put(sibling.getLastChild().getTextContent(), sibling.getTextContent());
		}
		
		return packname;
	}
	
	public void getDiffs() {
		getDiffs("function");
		getDiffs("constructor");
	}
	
	public void getDiffs(String tagName) {
		ArrayList<String> firstMethods = new ArrayList<String>();
		ArrayList<String> nextMethods = new ArrayList<String>();
		NodeList fNodeMethods = firstDoc.getElementsByTagName(tagName);
		NodeList nNodeMethods = nextDoc.getElementsByTagName(tagName);
		
		firstMethods = getMethods(firstPackageName, firstImportedTypes, fNodeMethods);
		nextMethods = getMethods(nextPackageName, nextImportedTypes, nNodeMethods);
		
		String firstMethodDesc;
		String nextMethodDesc;

		for (int i = 0; i < nNodeMethods.getLength(); i++) {
			nextMethodDesc = nextMethods.get(i);
			try {
				boolean inFirst = false;
				for (int j = 0; j < fNodeMethods.getLength(); j++) {
					firstMethodDesc = firstMethods.get(j);	
					if (firstMethodDesc.compareTo(nextMethodDesc)==0) {
						inFirst = true;
						if (!checkMethodDifferences(fNodeMethods.item(j), nNodeMethods.item(i))) {
							allChangedMethods.add(nextMethods.get(i));
						}
						break;
					}
				}
				if (!inFirst) {
					// methods added in the next revision relative to the first revision
					allAddedMethods.add(nextMethods.get(i));
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		// methods deleted from the first revision
		for (int i = 0; i < fNodeMethods.getLength(); i++) {
			firstMethodDesc = firstMethods.get(i);
			
			boolean inNext = false;
			for (int j = 0; j < nNodeMethods.getLength(); j++) {
				nextMethodDesc = nextMethods.get(j);	
				if (firstMethodDesc.compareTo(nextMethodDesc)==0) {
					inNext = true;
					break;
				}
			}
			if (!inNext) {
				allDeletedMethods.add(firstMethods.get(i));
			}
		}
	}
	public Node getClassParentRecursive(Node node) {
		Node parent = node.getParentNode();
		if (null == parent || (parent instanceof Document)) {
			return null;
		}
		
		if (parent instanceof Node && 
			parent.getNodeName().trim().compareTo("class")==0) {
			return (Node)parent;
		}
		
		// recurse upward
		return getClassParentRecursive(parent);
	}
	
	public Node getClassParent(Node node) {
		if (null == node) {
			return null;
		}
		Node parent = node.getParentNode();

		while (null != parent && parent instanceof Node && 
			!(parent.getNodeName().trim().compareTo("class")==0 &&
			parent.getTextContent().trim().contains("class "))) {
			/* debugging ...
			System.err.println("parent= " + parent.getNodeType());
			*/	
			parent = parent.getParentNode();
		}
		
		if (null == parent || !(parent instanceof Node)) {
			/* debugging ...
			if (null == parent) 
				System.err.println("yes, died here with null parent for node=." + node.getTextContent());
			else System.err.println("yes, died here with parent=" + parent.getTextContent());
			*/
			return null;
		}
		
		return parent;
	}
	
	/** retrieve and construct the full class name of a given method, including all levels of embedding classes and anonymous classes if applicable */  
	private int anonyclassIdx = 0, preIdx = 0; // simulate a stack for the recursive class-nesting analysis
	private String clsName = "", preName = ""; // simulate a stack for the recursive class-nesting analysis
	private String getClassName(Node curnode) {
		String ret="";
		if (null == curnode || curnode instanceof Document) {
			return ret;
		}
		//Node cSibling = getClassParent(targetMethod.getFirstChild().getNextSibling());
		Node cSibling = getClassParentRecursive(curnode);
		if (null == cSibling) {
			return ret;
		}
		boolean isAnonymous = false;
		Node prenode = cSibling.getPreviousSibling();
		if (prenode != null && prenode.getTextContent().trim().compareTo("new")==0) {
			isAnonymous = true;
		}
		
		cSibling = getClassParent(/*isAnonymous?cSibling:*/curnode);
		
		String prefix = getClassName(cSibling);
		/** according to a long debugging with Ant-v2 (Javadoc.java), this is most likely to be a bug of srcXML: the closing tag </class> for the first <class> tag is misplaced!*/
		if (null == cSibling) {
			System.err.println("!!! Warning !!! : cannot locate the parent <class> node of the following node; will use the top-level class as its parent \n" 
					+ curnode.getTextContent());
			cSibling = curnode.getOwnerDocument().getElementsByTagName("class").item(0);
			/* debugging ...
			cSibling = cSibling.getFirstChild().getNextSibling();
			while (cSibling != null && !(cSibling instanceof Element && cSibling.getNodeName().compareTo("name")==0)) {
				cSibling = cSibling.getNextSibling();
			}
			String curClassName = cSibling.getTextContent().trim();
			System.err.println("succeeded: will use " + curClassName);System.exit(0);
			*/
		}
		
		/* debugging ...
		try {
			cSibling = cSibling.getFirstChild().getNextSibling();
		}catch(Exception e) {
			System.err.println("cannot locate parent node of the node " + curnode.getTextContent() + " is anonymous=" + isAnonymous);
			e.printStackTrace();
			System.err.println(cSibling.getTextContent());
			System.exit(-1);
		}
		*/
		cSibling = cSibling.getFirstChild().getNextSibling();
		while (cSibling != null && !(cSibling instanceof Element && cSibling.getNodeName().compareTo("name")==0)) {
			cSibling = cSibling.getNextSibling();
		}
		String curClassName = cSibling.getTextContent().trim();
		if (isAnonymous) {
			if (clsName=="") {
				// (first) stack push 
				clsName = curClassName;
			}
			else {
				if (clsName.compareTo(curClassName)!=0) {
					if (preName.compareTo(curClassName)==0) {
						// stack pop
						clsName = preName;
						anonyclassIdx = preIdx;
					}
					else {
						// stack push
						preName = clsName;
						clsName = curClassName;
						preIdx = anonyclassIdx;
						anonyclassIdx = 0;
					}
				}
				else {
					anonyclassIdx ++;
				}
			}
		}
		/*
		if (className.compareTo("()")==0) {
			// anonymous class
			cSibling = getClassParent(curnode.getParentNode().getParentNode());
			cSibling = cSibling.getFirstChild().getNextSibling();
			while (cSibling != null && !(cSibling instanceof Element)) {
				cSibling = cSibling.getNextSibling();
			}
			className = cSibling.getTextContent() + "$" + anonyclassIdx;
			anonyclassIdx ++;
		}
		else {
			anonyclassIdx = 0;
		}
		*/
		if (isAnonymous) {
			curClassName += "$" + anonyclassIdx;
		}
		
		if (prefix.length()>=1) {
			prefix += "$";
		}
		
		return ret = prefix + curClassName;
	}
	
	/** retrieve the full-qualified types for return values and parameters in order to make up the <b>signature</b> of a method */
	private String getFullQualifiedType(Map<String, String> importedTypes, String typeName) {
		String fullTypeName = "";

		if (importedTypes.containsKey(typeName)) {
			fullTypeName = importedTypes.get(typeName);
		}
		else {
			// try to get the full-qualified type name with the three most commonly used Java legacy packages java.lang, java.io and java.util. 
			Class<?> t;
			try {
				t = Class.forName("java.lang."+typeName, true, Thread.currentThread().getContextClassLoader());
				fullTypeName = t.getCanonicalName();
			} catch (ClassNotFoundException e) {
				try {
					t = Class.forName("java.io."+typeName, true, Thread.currentThread().getContextClassLoader());
					fullTypeName = t.getCanonicalName();
				} catch (ClassNotFoundException e1) {
					try {
						t = Class.forName("java.util."+typeName, true, Thread.currentThread().getContextClassLoader());
						fullTypeName = t.getCanonicalName();
					} catch (ClassNotFoundException e2) {
						fullTypeName = typeName;
					}
				}
			}
		}
		
		return fullTypeName;
	}
	
	/** for each XML Node in the given list that describes a method, retrieve the signature of that method */
	public ArrayList<String> getMethods(String packname, Map<String, String> importedTypes, NodeList nodeMethods) {
		ArrayList<String> nextMethods = new ArrayList<String>();

		if (packname.length()>=1) {
			packname += ".";
		}
		preName = clsName = "";
		preIdx = anonyclassIdx = 0;
		
		for (int i = 0; i < nodeMethods.getLength(); i++) {
			Node targetMethod = nodeMethods.item(i);
			Node sibling = targetMethod.getFirstChild();
			while (sibling != null && !(sibling instanceof Element)) {
				  sibling = sibling.getNextSibling();
			}
			
			// ignore specifiers (of permission type---public/protected/private; accession mode---static/final;)
			boolean isCtor = sibling.getNodeName().trim().compareTo("type")!=0; // an nonctor function should have first its 'type' child node
			boolean isRetArray = sibling.getLastChild().getNodeName().trim().compareTo("index")==0;
			/*
			if (!isCtor) {
				while (sibling.hasChildNodes() && sibling.getFirstChild().getNodeName().trim().compareTo("specifier")==0) {
					sibling.removeChild(sibling.getFirstChild());
				}
			}
			sibling = sibling.getFirstChild();
			while (sibling != null && !(sibling instanceof Element)) {
				  sibling = sibling.getNextSibling();
			}
			String retType = sibling.getTextContent(); //sibling.getLastChild().getTextContent();
			*/
			String retType = "";
			if (isRetArray) {
				Node pSibling = sibling.getLastChild().getPreviousSibling();
				while (pSibling != null && !(pSibling instanceof Element)) {
					  pSibling = pSibling.getPreviousSibling();
				}
				retType = pSibling.getTextContent().trim();
			}
			else {
				retType = sibling.getLastChild().getTextContent().trim();
			}
			
			/*
			if (importedTypes.containsKey(retType)) {
				// replace with the full-qualified type
				retType = importedTypes.get(retType);
			}
			*/
			retType = getFullQualifiedType(importedTypes, retType);
			retType = replaceInGenericType(importedTypes, retType);
			if (isRetArray) {
				retType += "[]";
			}
			
			sibling = sibling.getNextSibling();
			while (sibling != null && !(sibling instanceof Element)) {
				  sibling = sibling.getNextSibling();
			}
			String methodName = sibling.getTextContent();
			String[] mnsegs = methodName.trim().split("\\s+");
			methodName = mnsegs[mnsegs.length-1].trim();
			
			if (isCtor) {
				retType = "void";
				methodName = "<init>";
			}
			
			String className = getClassName(targetMethod);
						
			ArrayList<String> parameters = getParameters(importedTypes, targetMethod);
			
			String allparams = "";
			for ( int j = 0; j < parameters.size(); j++) {
				allparams += parameters.get(j);
				if (j < parameters.size()-1) {
					allparams += ",";
				}
			}
			
			// now concoct the full-qualified function prototype --- the signature of the method as if it were given by the program analysis process
			nextMethods.add("<" + packname + className + ": " + retType + " " + methodName + "(" + allparams + ")>");	
		}
		return nextMethods;
	}
	
	public boolean checkMethodDifferences(Node firstMethod, Node secondMethod){
		String firstText = new String(firstMethod.getTextContent());
		String secondText = new String(secondMethod.getTextContent());
		return (firstText.compareTo(secondText)==0);
	}
	
	public ArrayList<String> getParameters(Map<String, String> importedTypes, Node method) {
		ArrayList<String> params = new ArrayList<String>();
		Node sibling = method.getFirstChild();
		while ((sibling != null && !(sibling instanceof Element)) || sibling.getNodeName().trim().compareTo("parameter_list")!=0) {
			sibling = sibling.getNextSibling();
		}
		Node paramList = sibling;

		if (paramList.hasChildNodes() && paramList.getNodeName().trim().compareTo("parameter_list")==0) {
			NodeList paramNodes = paramList.getChildNodes();
			for (int i = 0; i < paramNodes.getLength(); i++) {
				Node curParaNode = paramNodes.item(i);
				if (curParaNode.getNodeName().trim().compareTo("param")!=0) {
					continue;
				}
				
				// replace each parameter type with the corresponding full-qualified type
				Node paraTypeNode = curParaNode.getFirstChild().getFirstChild();
				assert paraTypeNode.getNodeName().trim().compareTo("type")==0;
				
				String paraType = paraTypeNode.getTextContent().trim();
				boolean isArray = false;
				if (paraType.endsWith("[]")) {
					paraType = paraType.substring(0, paraType.lastIndexOf("[]"));
					isArray = true;
				}
				/*
				if (importedTypes.containsKey(paraType)) {
					paraType = importedTypes.get(paraType);
				}
				else {
					// try to get the full-qualified type name with the three most commonly used Java legacy packages java.lang, java.io and java.util. 
					Class<?> t;
					try {
						t = Class.forName("java.lang."+paraType, true, Thread.currentThread().getContextClassLoader());
						paraType = t.getCanonicalName();
					} catch (ClassNotFoundException e) {
						try {
							t = Class.forName("java.io."+paraType, true, Thread.currentThread().getContextClassLoader());
							paraType = t.getCanonicalName();
						} catch (ClassNotFoundException e1) {
							try {
								t = Class.forName("java.util."+paraType, true, Thread.currentThread().getContextClassLoader());
								paraType = t.getCanonicalName();
							} catch (ClassNotFoundException e2) {
							}
						}
					} 
				}
				*/
				paraType = getFullQualifiedType(importedTypes, paraType);
				paraType = replaceInGenericType(importedTypes, paraType);
				if (isArray) {
					paraType += "[]";
				}
				
				Node pSibling = paraTypeNode.getNextSibling();
				while (pSibling != null && !(pSibling instanceof Element)) {
					pSibling = pSibling.getNextSibling();
				}
				//String paraName = pSibling.getTextContent().trim();
				
				/** a parameter name is usually not present as part of the signature of the host function */
				//params.add(paraType + " " + paraName);
				params.add(paraType);
			}
			if (params.size() == 0) params.add("");
			return params;
		}
		
		params.add("");
		return params;
	}
	
	/** replace each type in a generic-type instantiation, such as HashSet<Integer, Object>, with its full-qualified name */
	private String replaceInGenericType(Map<String, String> importedTypes, String type) {
		String sret = type;
		//String[] tokenVals = type.split("\\<|,\\s+|\\>");
		String[] tokenVals = type.split("\\<"); // Soot takes the generic type and discards the parameters when producing the signature
		if (tokenVals.length < 2) {
			return sret;
		}
		/*
		for (String token : tokenVals) {
			sret = sret.replace(token, getFullQualifiedType(importedTypes, token));
		}
		*/
		sret = getFullQualifiedType(importedTypes, tokenVals[0]);
		return sret;
	}
}

/* vim :set ts=4 tw=4 tws=4 */

