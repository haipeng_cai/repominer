
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Date;
import java.util.Map;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.JarInputStream;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;

import java.io.File;


public class XMLMethodDiff {
	public static final List<String> allChangedMethods = new ArrayList<String>(); // hcai added
	
	public String className;
	public File firstFile;
	public File nextFile;
	public File outFile;
	public Document nextDoc;
	public Document firstDoc;
	public XMLMethodDiff(File firstFile, File nextFile) {
		
		try {
			this.firstFile = firstFile;
			this.nextFile = nextFile;
			
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			this.firstDoc = dBuilder.parse(firstFile);	
			this.nextDoc = dBuilder.parse(nextFile);
			
			outFile = new File(nextFile.getAbsolutePath() + ".diff");
			
			NodeList classNames = nextDoc.getElementsByTagName("class");
			for (int i = 0; i < classNames.getLength(); i++){
				Node sibling = classNames.item(i).getFirstChild().getNextSibling();
				while (!(sibling instanceof Element) && sibling != null) {
					  sibling = sibling.getNextSibling();
				}
				className = sibling.getTextContent();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	
	}
	public void getDiffs(){
		
		ArrayList<String> firstMethods = new ArrayList<String>();
		ArrayList<String> nextMethods = new ArrayList<String>();
		NodeList fNodeMethods = firstDoc.getElementsByTagName("function");
		NodeList nNodeMethods = nextDoc.getElementsByTagName("function");
		
		firstMethods = getMethods(fNodeMethods);
//		for (int i = 0; i < firstMethods.size(); i++){
//			System.out.println(firstMethods.get(i));
//		}
		String firstMethodDesc;
		String secondMethodDesc;
		/*
		 * Error: check sizes of nextMethods, firstMethods, nNodeMethods, fNodeMethods - out of bounds errors may be the issue!
		 * debug inside for loops, immediately before checkMethodDifferences call
		 * 
		 * try traversing over fNodeMethod, nNodeMethod instead
		 */
		nextMethods = getMethods(nNodeMethods);
		for (int i = 0; i < nNodeMethods.getLength(); i++){
			try {
				FileWriter outWriter = new FileWriter(outFile, true);
			boolean inFirst = false;
			for (int j = 0; j < fNodeMethods.getLength(); j++){
				firstMethodDesc = firstMethods.get(j);	
				secondMethodDesc = nextMethods.get(i);
				if (firstMethodDesc.equals(secondMethodDesc)) {
					inFirst = true;
					if (!checkMethodDifferences(fNodeMethods.item(j), nNodeMethods.item(i))){ /** HCAI's test CRASHED HERE! Hint: nextMethods.size() may not be equal to nNodeMethods.size() */
						outWriter.write(nextMethods.get(i));
						allChangedMethods.add(nextMethods.get(i));// hcai added
						break;
					}
				}
			}
			if (!inFirst) {
				outWriter.write(nextMethods.get(i));
				allChangedMethods.add(nextMethods.get(i));// hcai added
			}
			outWriter.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			//System.out.println(nextMethods.get(i));
		}
//		System.out.println("");		
		
	}
	
	public ArrayList<String> getMethods(NodeList nodeMethods){
		ArrayList<String> nextMethods = new ArrayList<String>();
		for (int i = 0; i < nodeMethods.getLength(); i++){
			Node sibling = nodeMethods.item(i).getFirstChild().getNextSibling();
			while (!(sibling instanceof Element) && sibling != null) {
				  sibling = sibling.getNextSibling();
			}
			Node targetMethod = nodeMethods.item(i);
			String methodName = sibling.getTextContent();
			
			//Get Class name
			//cSibling is used to determine name of a class node
			Node cSibling = getClassParent(nodeMethods.item(i)).getFirstChild().getNextSibling();
			while (!(cSibling instanceof Element) && cSibling != null) {
				  cSibling = cSibling.getNextSibling();
			}
			className = cSibling.getTextContent();
			
			//System.out.println(className + ": " + methodName);
			ArrayList<String> parameters = getParameters(targetMethod);
			
			if (parameters != null){
				for ( int j = 0; j < parameters.size(); j++){
					parameters.set(j, className + ": " + methodName + " (" + parameters.get(j) + ") \n");
					nextMethods.add(parameters.get(j));
					
				}
			}
		}
		return nextMethods;
	}
	
	public boolean checkMethodDifferences(Node firstMethod, Node secondMethod){
		String firstText = new String(firstMethod.getTextContent());
		String secondText = new String(secondMethod.getTextContent());
		return (firstText.equals(secondText));
	}
	
	public ArrayList<String> getParameters(Node method) {
		
		ArrayList<String> params = new ArrayList<String>();
//		String PLIST[] = {"parameter_list"};
		Node sibling = method.getFirstChild();
		while ((!(sibling instanceof Element) && sibling != null) || sibling.getNodeName() != "parameter_list") {
			//System.out.println(sibling.getNodeName());  
			sibling = sibling.getNextSibling();
		}
		Node paramList = sibling;

		if (paramList.hasChildNodes() && paramList.getNodeName() == "parameter_list"){
			NodeList paramNodes = paramList.getChildNodes();
			for (int i = 0; i < paramNodes.getLength(); i++){
				if (paramNodes.item(i).getNodeName() == "param"){
//					System.out.println(paramNodes.item(i).getTextContent());
					params.add(paramNodes.item(i).getTextContent());
				}
			}
			if (params.size() == 0) params.add("void");
			return params;
		}
			params.add("void");
			return params;
		
	}
	
	//nonworking attempt to single out parameter_list elements - will return to it
	public static Node getFirstChildElement(Node parent, String[] strings) {
	      
	      // search for node
			Node child = parent.getFirstChild();
			while (child != null) {
				if (child.getNodeType() == Node.ELEMENT_NODE) {
					System.out.println(child.getNodeName());
					for (int i = 0; i < strings.length; i++) {
						if (child.getNodeName().equals(strings[i])) {
							return (Node)child;
	                }
	            }
	        }
	        child = child.getNextSibling();
			}
	      	// not found
	    	System.out.println("failed to get parameter list");
	    	return null;
	  }
	
	public Node getClassParent(Node node){
		Node parent = node.getParentNode();
	      if (parent instanceof Node && parent.getNodeName() == "class"){
	          return (Node)parent;
	      }else{
	    	  return getClassParent(parent);
	      }
	}
}
