import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Date;
import java.util.Map;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.JarInputStream;
import java.util.regex.Pattern;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import org.tmatesoft.svn.core.SVNDirEntry;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNLogEntry;
import org.tmatesoft.svn.core.SVNLogEntryPath;
import org.tmatesoft.svn.core.SVNNodeKind;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.auth.ISVNAuthenticationManager;
import org.tmatesoft.svn.core.internal.io.dav.DAVRepositoryFactory;
import org.tmatesoft.svn.core.internal.io.fs.FSRepositoryFactory;
import org.tmatesoft.svn.core.internal.io.svn.SVNRepositoryFactoryImpl;
import org.tmatesoft.svn.core.io.SVNRepository;
import org.tmatesoft.svn.core.io.SVNRepositoryFactory;
import org.tmatesoft.svn.core.wc.SVNClientManager;
import org.tmatesoft.svn.core.wc.SVNRevision;
import org.tmatesoft.svn.core.wc.SVNUpdateClient;
import org.tmatesoft.svn.core.wc.SVNWCUtil;
import org.tmatesoft.svn.core.wc2.SvnCheckout;
import org.tmatesoft.svn.core.wc2.SvnOperationFactory;
import org.tmatesoft.svn.core.wc2.SvnTarget;

import de.srcml.parser.Parser;

public class DisplayRepoTree {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// - hcai added
		if (args.length < 4) {
			System.err.println("too few arguments: DisplayRepoTree url user_name password target_dir");
			return;
		}
		// -
		// TODO Auto-generated method stub
		DAVRepositoryFactory.setup();
		SVNRevision firstRevision;
		SVNRevision nextRevision;
		/* hcai commented out
		String url = "https://subversion.assembla.com/svn/jmrepotest/";
		String name = "jmagiera";
		String password = "********";
		String repoPath = "/Users/jmagiera/testing/repotest/";
		long start = 0;
		long end = 8;
		*/
		// - hcai added
		String url = args[0];
		String name = args[1];
		String password = args[2];
		String repoPath = args[3];
		boolean checkOut = false;
		
		long start = -1;
		long end = -1;
		long nRevisions=7;
		
		if (args.length == 5) {
			nRevisions = Long.parseLong(args[4]);
		}
		if (args.length > 5) {
			if (args[5] == "-d"){
				nRevisions = Long.parseLong(args[4]);
				checkOut = true;
			} else if (args.length == 7 && args[6] == "-d"){
				checkOut = true;
				start = Long.parseLong(args[4]);
				end = Long.parseLong(args[5]);
			} else {
				checkOut = false;
				start = Long.parseLong(args[4]);
				end = Long.parseLong(args[5]);
			}
			
		}
		// -
		
		boolean isRecursive = true;
		setupLibrary();
		
		SVNRepository repository = null;
		try {
			  repository = SVNRepositoryFactory.create( SVNURL.parseURIEncoded( url ) );
			  ISVNAuthenticationManager authManager = SVNWCUtil.createDefaultAuthenticationManager( name, password );
			  repository.setAuthenticationManager( authManager );
			  final SvnOperationFactory svnOperationFactory = new SvnOperationFactory();
			  
			  Collection logEntries = null;
			  
			  SVNNodeKind nodeKind = repository.checkPath("",-1);
			  if (nodeKind == SVNNodeKind.NONE){
				  	System.err.println("No entry at " + url + ".");
				  	System.exit(1);
			  } else if (nodeKind == SVNNodeKind.FILE){
				  	System.err.println("The entry at "+ url +"is a file, not a directory.");
				  	System.exit(1);
			  }
			  
			  // end = repository.getLatestRevision();
			  // - hcai added
			  long latest = repository.getLatestRevision(), earliest = 0;
			  if (-1 == end) {
				  end = latest;
				  start = end - nRevisions;
			  }
			  if (start < earliest) start = earliest;
			  if (end > latest) end = latest;
			  // -
			  
			  for (long i = start; i < end + 1; i++){
				  // - hcai added
				  if (checkOut) System.err.print("Downloading revision " + url + " " + i + " ...... ");
				  long sttime = System.currentTimeMillis();
				  // -
				  try {
					  	File FirstDestPath = new File(repoPath + Long.toString(i));
					  	if (FirstDestPath.exists()) {
					  		if (checkOut) System.err.println("Files already exist, skipped.");
				  		}else {
				  			FirstDestPath.mkdirs();
				  			
				  			final SvnCheckout checkout = svnOperationFactory.createCheckout();
							checkout.setSingleTarget(SvnTarget.fromFile(FirstDestPath));
							checkout.setSource(SvnTarget.fromURL(SVNURL.parseURIEncoded(url)));
						    //... other options
							firstRevision = SVNRevision.create(i);
							if (firstRevision.isValid() && checkOut){
								checkout.setRevision(firstRevision);
								checkout.run();
							}
							else {
								System.err.println("Stopped: reached last revision");
								// break;
							}
				  		}
				  } finally {
					  svnOperationFactory.dispose();
				  }
				  // - hcai added
				  System.err.println("Done. [" + (System.currentTimeMillis()-sttime)/1000 + " seconds]");
				  
				  System.err.print("Differencing revision " + url + " " + i + " ...... ");
				  sttime = System.currentTimeMillis();
				  XMLMethodDiff.allChangedMethods.clear();
				  // -
				  
				  convertContents(repository,"",repoPath+Long.toString(i));
				  // if (i > 0){ // hcai changed
				  if (i > start) {	  
					String lastPath = repoPath+Long.toString(i-1); 
					try {
						findDiffs(repository,"",lastPath,repoPath+Long.toString(i));
						// - hcai added
						/** report all changed methods between these two revisions */
						System.out.println("changed methods between revision " + (i-1) + " and " + i + " :");
						for (String m : XMLMethodDiff.allChangedMethods) {
							System.out.println(m);
						}
						// -
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				  }
				  System.err.println("Done. [" + (System.currentTimeMillis()-sttime)/1000 + " seconds]"); // hcai added
			  }
			  //SVNClientManager clientManager = SVNClientManager.newInstance(null,repository.getAuthenticationManager());
			  //SVNUpdateClient updateClient = clientManager.getUpdateClient();
			  //updateClient.doCheckout(SVNURL.parseURIEncoded(url),destPath,revision,revision,isRecursive);
			  
		} catch (SVNException svne) {
			System.err.println("error while creating an SVNRepository for the location '"
                    + url + "': " + svne.getMessage());
			System.exit(1);
		}
	}
	
	public static void findDiffs(SVNRepository repository, String path, String firstPath, String nextPath) throws SVNException, IOException {
		//File diffFile = new File(firstPath + "/" + path);
		
		Collection entries = repository.getDir(path,-1,null,(Collection)null);
		Iterator iterator = entries.iterator();
		String command;
		while (iterator.hasNext()){
			SVNDirEntry entry = (SVNDirEntry)iterator.next();
			String firstFilePath = firstPath + "/" + path + "/" + entry.getName();
			String nextFilePath = nextPath + "/" + path + "/" + entry.getName();
			//System.out.println("/" + (path.equals("") ? "" : path+"/") + entry.getName());
			if (entry.getKind() == SVNNodeKind.DIR) {
				findDiffs(repository, (path.equals("") ? entry.getName() : path + "/" + entry.getName()), firstPath, nextPath);
			} else if (entry.getName().endsWith(".java")) {
				//System.out.println(entry.getName());
				/*File diffFile = new File(nextFilePath + ".xmldiff");
				
				if (!diffFile.exists()){
					diffFile.createNewFile();
				}
				*/
				
				try {
					if (new File(firstFilePath + ".xml").exists() && new File(nextFilePath + ".xml").exists()){
						System.out.println(firstFilePath + ", " + nextFilePath);
						XMLMethodDiff xmlDiff = new XMLMethodDiff(new File(firstFilePath + ".xml"), new File(nextFilePath + ".xml"));
						xmlDiff.getDiffs();
					}
				}
				catch (Exception err) {
				      err.printStackTrace();
				}
				
			}
		}
	}
	
	public static void convertContents(SVNRepository repository, String path, String repoPath) throws SVNException{
		Collection entries = repository.getDir(path,-1,null,(Collection)null);
		Iterator iterator = entries.iterator();
		String command;
		//Parser parser = new Parser();
		while (iterator.hasNext()){
				SVNDirEntry entry = (SVNDirEntry)iterator.next();
				//System.out.println("/" + (path.equals("") ? "" : path+"/") + entry.getName());
				if (entry.getKind() == SVNNodeKind.DIR) {
					convertContents(repository, (path.equals("") ? entry.getName() : path + "/" + entry.getName()), repoPath);
				} else if (new File(repoPath + "/" + path + "/" + entry.getName()).exists()) {
					command = "./libs/src2srcml "+ repoPath + "/" + path + "/" + entry.getName() + " -o " + repoPath + "/" + path + "/" + entry.getName() + ".xml";
					System.out.println(command);
					try {
						Process myProcess = Runtime.getRuntime().exec(command);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
		}
	}
	
	
    private static void setupLibrary() {
        /*
         * For using over http:// and https://
         */
        DAVRepositoryFactory.setup();
        /*
         * For using over svn:// and svn+xxx://
         */
        SVNRepositoryFactoryImpl.setup();
        
        /*
         * For using over file:///
         */
        FSRepositoryFactory.setup();
    }

}
