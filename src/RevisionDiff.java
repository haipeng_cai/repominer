/**
 * File: src/RevisionDiff.java
 * -------------------------------------------------------------------------------------------
 * Date			Author      Changes
 * -------------------------------------------------------------------------------------------
 * 03/31/14		hcai		created; the entry (controller) of the RepoMiner tool
 * 04/01/14		hcai		refactored and restructured the main steps for finding different methods between revisions
 * 04/03/14		hcai		reached the fully working version
 * 04/04/14		hcai		fixed a few SVN connection issues 
 * 04/05/14		hcai		enforced successful execution of each single step on each source code file --- immediately exit on the occurrence of any exception 
 * 04/06/14		hcai		clear the lists of added/removed methods before differencing each pair of revisions
*/
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import org.tmatesoft.svn.core.SVNDirEntry;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNNodeKind;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.auth.ISVNAuthenticationManager;
import org.tmatesoft.svn.core.internal.io.dav.DAVRepositoryFactory;
import org.tmatesoft.svn.core.internal.io.fs.FSRepositoryFactory;
import org.tmatesoft.svn.core.internal.io.svn.SVNRepositoryFactoryImpl;
import org.tmatesoft.svn.core.io.SVNRepository;
import org.tmatesoft.svn.core.io.SVNRepositoryFactory;
import org.tmatesoft.svn.core.wc.SVNRevision;
import org.tmatesoft.svn.core.wc.SVNWCUtil;
import org.tmatesoft.svn.core.wc2.SvnCheckout;
import org.tmatesoft.svn.core.wc2.SvnOperationFactory;
import org.tmatesoft.svn.core.wc2.SvnTarget;

public class RevisionDiff {
	static final RepoOptions opts = new RepoOptions();
	
	static List<Long> revList = new ArrayList<Long>();
	static Map<Long, Long> revPairs = new LinkedHashMap<Long, Long>();
	static SVNRepository repository = null;
	static SvnOperationFactory svnOperationFactory = null;
	
	private static int initRepo() {
		/** For using over http:// and https:// */
        DAVRepositoryFactory.setup();
        
        /** For using over svn:// and svn+xxx:// */
        SVNRepositoryFactoryImpl.setup();
        
        /** For using over file:/// */
        FSRepositoryFactory.setup();
        
		try {
			repository = SVNRepositoryFactory.create( SVNURL.parseURIEncoded( opts.url() ) );
			ISVNAuthenticationManager authManager = SVNWCUtil.createDefaultAuthenticationManager( opts.name(), opts.password());
			repository.setAuthenticationManager( authManager );
			svnOperationFactory = new SvnOperationFactory();
		}
		catch (SVNException svne) {
			System.err.println("error occurred while initializing/setting up the repository at "
                + opts.url() + ": \n\t" + svne.getMessage());
			return -1;
		}
		return 0;
	}
	
	private static int getRevisionList() {
		File srcFile = new File(opts.fnRevList());

		if (!srcFile.isFile()) {
			System.err.println("[ERROR] file " + opts.fnRevList() + " does not exist!");
			return -1;
		}

		try {
			BufferedReader reader = new BufferedReader(new FileReader(srcFile));
			String s = reader.readLine();
			int lineCounter = 0;

			while (s != null) {
				revList.add(Long.parseLong(s.trim()));

				lineCounter++;
				s = reader.readLine();
			}

			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
			return -1;
		}
		return 0;
	}
	
	private static int getRevisionPairs() {
		File srcFile = new File(opts.fnRevPairs());

		if (!srcFile.isFile()) {
			System.err.println("[ERROR] file " + opts.fnRevPairs() + " does not exist!");
			return -1;
		}

		try {
			BufferedReader reader = new BufferedReader(new FileReader(srcFile));
			String s = reader.readLine();
			int lineCounter = 0;

			while (s != null) {
				String[] tokenVals = s.trim().split("\\s+|,\\s*|\\.\\s*");
				if (tokenVals.length < 2) {
					// skip unrecognizable lines
					s = reader.readLine();
					continue;
				}
				revPairs.put(Long.parseLong(tokenVals[0]), Long.parseLong(tokenVals[1]));
				revList.add(Long.parseLong(tokenVals[0]));
				revList.add(Long.parseLong(tokenVals[1]));

				lineCounter++;
				s = reader.readLine();
			}

			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
			return -1;
		}
		return 0;
	}
	
	/** if already specified, only specified revisions are downloaded; otherwise, attempt to download $nRev$ revisions starting from $startRev$ with interval of 1 */
	private static int Download() {
		int szRevs = revList.size();
		
		String url = opts.url();
		String repoPath = opts.repoPath();
		
		long start = opts.startRev();
		long end = opts.endRev();
		long nRevisions = opts.nRev();
		
		try {
			SVNNodeKind nodeKind = repository.checkPath("",-1);
			if (nodeKind == SVNNodeKind.NONE){
				System.err.println("No entry at " + url + ".");
				System.exit(1);
			} 
			else if (nodeKind == SVNNodeKind.FILE){
				System.err.println("The entry at "+ url +"is a file, not a directory.");
				System.exit(1);
			}
	  
			boolean isListSpecified = szRevs >= 1;
			if (szRevs < 1) {
				long latest = repository.getLatestRevision(), earliest = 0;
				if (-1 == end) {
				  end = latest;
				  start = end - nRevisions;
				}
				if (start < earliest) start = earliest;
				if (end > latest) end = latest;
				  
				szRevs = (int) (end - start + 1);
			}
			  			  
			for (int i = 0; i < szRevs; i++) {
				long curRev = (isListSpecified ? revList.get(i) : (start+i)); 
				System.err.println("Downloading revision " + url + " " + curRev + " ...... ");
			  				  
				long sttime = System.currentTimeMillis();
				try {
					File FirstDestPath = new File(repoPath + File.separator + Long.toString(curRev));
				  	if (FirstDestPath.exists()) {
				  		System.err.println("Skipped.");
				  	} 
				  	else {
				  		FirstDestPath.mkdirs();
				
				  		final SvnCheckout checkout = svnOperationFactory.createCheckout();
				  		checkout.setSingleTarget(SvnTarget.fromFile(FirstDestPath));
				  		checkout.setSource(SvnTarget.fromURL(SVNURL.parseURIEncoded(url)));
	
				  		SVNRevision svnRevision = SVNRevision.create(curRev);
				  		if (svnRevision.isValid()){
				  			checkout.setRevision(svnRevision);
				  			checkout.run();
				  		}
				  		else {
				  			System.err.println("Stopped: reached the last revision");
							return -1;
						}
			  		}
				  	if (!isListSpecified) { revList.add(curRev); }
				} 
				finally {
					svnOperationFactory.dispose();
				}
				System.err.println("Done. [" + (System.currentTimeMillis()-sttime)/1000 + " seconds]");
			  }
		}
		catch (SVNException svne) {
			System.err.println("error occurred while downloading from the repository "
                + opts.url() + ": \n\t" + svne.getMessage());
			return -2;
		}
		
		return 0;
	} // Download
	
	private static String getConvertedRepoPath(long curRev) {
		 String repoConvedPath = opts.repoPath();
		  if (repoConvedPath.charAt(repoConvedPath.length()-1) == File.separatorChar) {
			  repoConvedPath = repoConvedPath.substring(0, repoConvedPath.length()-1);
		  }
		  repoConvedPath += "_converted" + File.separator + Long.toString(curRev);
		  
		  return repoConvedPath;
	}
	
	private static void reportResults(long frev, long nrev, List<String> added, List<String> deleted) {
		System.out.println("\n\n======= Differences between revision " + frev + " and " + nrev + " =======");
		System.out.println("[ changed methods ]"); 
		for (String m : SrcXMLDiff.allChangedMethods) {
			System.out.println(m);
		}
		System.out.println("[ added methods ]");
		for (String m : SrcXMLDiff.allAddedMethods) {
			System.out.println(m);
		}
		System.out.println("[ deleted methods ]");
		for (String m : SrcXMLDiff.allDeletedMethods) {
			System.out.println(m);
		}
		System.out.println("[ added files (classes) ] :");
		for (String c : added) {
			System.out.println(c);
		}
		System.out.println("[ deleted files (classes) ] :");
		for (String c : deleted) {
			System.out.println(c);
		}
		System.out.println("=================================================================");
		
		// write differences---changed and deleted methods for now for predictive CIA---into a file under the current directory if specified
		if (opts.outputDiff()) {
			String fnOut = System.getProperty("user.dir") + File.separator + "diff_" + frev + "-" + nrev;
			try {
				FileWriter fw = new FileWriter(new File(fnOut));
				for (String m : SrcXMLDiff.allChangedMethods) {
					fw.write(m+"\n");
				}
				for (String m : SrcXMLDiff.allDeletedMethods) {
					fw.write(m+"\n");
				}
				/** specially mark deleted classes --- we can, but do not right now, retrieve all methods from each of those classes and as deleted methods */
				if (!deleted.isEmpty()) {
					fw.write("*** deleted classes ***\n");
				}
				for (String c : deleted) {
					fw.write(c+"\n");
				}
				fw.flush();
				fw.close();
			} catch (IOException e) {
				e.printStackTrace();
			} 
		}
	} // reportResults
	
	public static void main(String[] args) {
		// command-line processing
		if (args.length < 4) {
			System.err.println("too few arguments: RevisionDiff url user_name password target_dir");
			return;
		}
		try {
			opts.process(args);
		}
		catch (Exception e) {
			e.printStackTrace();
			return;
		}
		
		// initialize the SVN repository according to the given URL
		if (0 != initRepo()) System.exit(-0xff);
		
		// determine which revisions to consider
		if (opts.fnRevPairs().length() > 1) {
			if (0 != getRevisionPairs()) System.exit(-1);
		}
		else if (opts.fnRevList().length() > 1) {
			if (0 != getRevisionList()) System.exit(-1);
		}
		else if (opts.startRev()!=-1 && opts.endRev()!=-1) {
			int i = 0;
			while ( i < opts.nRev() && (opts.startRev()+i <= opts.endRev()) ) {
				revList.add(opts.startRev()+i);
				i++;
			}
		}
		else {
			System.err.println("no revisions on which to work.");
			System.exit(-3);
		}
		
		if (revPairs.isEmpty()) {
			/** if pairs for comparison are not specified explicitly through the fnRevPairs argument, adopt every two consecutive revisions as pairs to compare */
			for (int i = 0; i < revList.size()-1; i++) {
				revPairs.put(revList.get(i), revList.get(i+1));
			}
		}
		
		System.err.println("to be working on the following pairs of revision:");
		for (Long frev : revPairs.keySet()) {
			System.err.println("\t " + frev + " <---> " + revPairs.get(frev));
		}
		
		// check out specified revisions from repository if necessary
		if (opts.checkout()) {
			if (0 != Download()) System.exit(-2);
		}
		
		// convert source-code files to XML-formatted files
		assert repository != null;
		assert svnOperationFactory != null;
		File fRepoConved = null;
		try {
			for (int i = 0; i < revList.size(); i++) {
			  long curRev = revList.get(i);
			  
			  System.err.println("Converting revision " + opts.url() + " " + curRev + " ...... ");
			  long sttime = System.currentTimeMillis();
			  
			  String repoConvedPath = getConvertedRepoPath(curRev);
			  fRepoConved = new File(repoConvedPath);
			  if (!fRepoConved.exists()) {
				  fRepoConved.mkdirs();
			  }
			  else {
				  // target directory exists, meaning that the revision has already been converted
				  System.err.println("Skipped.");
				  continue;
			  }
			  convertContents(repository, "", curRev, opts.repoPath() + File.separator + Long.toString(curRev), repoConvedPath);
			  
			  System.err.println("Done. [" + (System.currentTimeMillis()-sttime)/1000 + " seconds]");

			  if (revPairs.isEmpty() && i > 0) {
				// String lastPath = opts.repoPath() + File.separator + Long.toString(revList.get(i-1));
				String lastPath = getConvertedRepoPath(revList.get(i-1));
				assert lastPath != null;
				try {
					System.err.println("Differencing revision " + curRev + " against revision " + revList.get(i-1));
					SrcXMLDiff.allChangedMethods.clear();
					SrcXMLDiff.allAddedMethods.clear();
					SrcXMLDiff.allDeletedMethods.clear();
					
					List<String> added = new ArrayList<String>(), deleted = new ArrayList<String>();
					sttime = System.currentTimeMillis();
					
					findDiffs(repository, "", curRev, lastPath, repoConvedPath, added, deleted);
					
					System.err.println("Done. [" + (System.currentTimeMillis()-sttime)/1000 + " seconds]");
					
					/** report all changed methods between these two revisions */
					reportResults(revList.get(i-1), revList.get(i), added, deleted);
					
				} catch (IOException e) {
					e.printStackTrace();
					System.exit(1);
				}
			  }
			}
		} 
		catch (SVNException svne) {
			System.err.println("error occurred while converting repository content to srcXML from '"
                    + opts.url() + "': \n\t" + svne.getMessage());
			if (null != fRepoConved) {
				try {delete(fRepoConved);} catch(Exception e) {}
			}
			System.exit(1);
		}
		
		// differentiate between revisions
		for (Map.Entry<Long, Long> en : revPairs.entrySet()) {
			String curPath = getConvertedRepoPath(en.getValue());
			String lastPath = getConvertedRepoPath(en.getKey());
			
			try {
				SrcXMLDiff.allChangedMethods.clear();
				SrcXMLDiff.allAddedMethods.clear();
				SrcXMLDiff.allDeletedMethods.clear();
				
				List<String> added = new ArrayList<String>(), deleted = new ArrayList<String>();
				System.err.println("\n\nDifferencing revision " + en.getKey() + " against revision " + en.getValue());
				long sttime = System.currentTimeMillis();
				
				findDiffs(repository, "", en.getKey(), lastPath, curPath, added, deleted);
				
				System.err.println("Done. [" + (System.currentTimeMillis()-sttime)/1000 + " seconds]");
				
				/** report all changed methods between these two revisions */
				reportResults(en.getKey(), en.getValue(), added, deleted);
			} 
			catch (IOException e) {
				e.printStackTrace();
				System.exit(1);
			}
			catch (SVNException svne) {
				System.err.println("error occurred while differencing between revisions from repository '"
	                    + opts.url() + "': \n\t" + svne.getMessage());
				System.exit(1);
			}
		}
	} // main
	
	/** convert source code to srcXML */
	public static void convertContents(SVNRepository repository, String path, long rev, String repoPath, String repoConvedPath) throws SVNException {
		Collection<?> entries = repository.getDir(path, rev, null, (Collection<?>)null);
		Iterator<?> iterator = entries.iterator();
		String command;

		while (iterator.hasNext()) {
			SVNDirEntry entry = (SVNDirEntry)iterator.next();
			//System.out.println("/" + (path.equals("") ? "" : path+"/") + entry.getName());
			if (entry.getKind() == SVNNodeKind.DIR) {
				convertContents(repository, (path.equals("") ? entry.getName() : path + File.separator + entry.getName()), rev, repoPath, repoConvedPath);
			} 
			else {
				File enfn = new File(repoPath + File.separator + path + File.separator + entry.getName());
				if (!enfn.exists()) {
					System.err.println(enfn.getAbsolutePath() + enfn.getName() + " does not exist, and thus is now skipped!");
					continue;
				}
				if (!entry.getName().endsWith(".java")) {
					// care about source-code changes only for now
					continue;
				}
				String targetPath = repoConvedPath + File.separator + path;
				File fnTarget = new File(targetPath);
				if (!fnTarget.exists()) {
					fnTarget.mkdirs();
				}
				command = opts.converterPath + " "+ repoPath + File.separator + path + File.separator + entry.getName() + 
						" -o " + targetPath + File.separator + entry.getName() + ".xml";
				System.err.println(command);
				try {
					Process myProcess = Runtime.getRuntime().exec(command);
					myProcess.waitFor();
					int ret = myProcess.exitValue();
					if (0 != ret) {
						System.err.println("src2srcml failed with " + repoPath + File.separator + path + File.separator + entry.getName() + " [retval=" + ret + "]");
					}
				} catch (IOException e) {
					e.printStackTrace();
					System.exit(1);
				} catch (InterruptedException e) {
					e.printStackTrace();
					System.exit(1);
				}
			}
		}
	} // convertContents
	
	/** differentiate between a pair of revisions */
	public static void findDiffs(SVNRepository repository, String path, long rev, String firstPath, String nextPath, List<String> added, List<String> deleted) 
		throws SVNException, IOException {
		Collection<?> entries = repository.getDir(path, rev, null, (Collection<?>)null);
		Iterator<?> iterator = entries.iterator();
		while (iterator.hasNext()) {
			SVNDirEntry entry = (SVNDirEntry)iterator.next();
			String firstFilePath = firstPath + File.separator + path + File.separator + entry.getName();
			String nextFilePath = nextPath + File.separator + path + File.separator + entry.getName();
			if (entry.getKind() == SVNNodeKind.DIR) {
				findDiffs(repository, (path.equals("")? entry.getName() : path + File.separator + entry.getName()), rev, firstPath, nextPath, added, deleted);
			} else if (entry.getName().endsWith(".java")) {
				try {
					File fFirstXml = new File(firstFilePath + ".xml");
					File fSecondXml = new File(nextFilePath + ".xml");
					System.err.println(firstFilePath + " vs " + nextFilePath);
					if (fFirstXml.exists() && fSecondXml.exists()) {
						SrcXMLDiff xmlDiff = new SrcXMLDiff(fFirstXml, fSecondXml);
						xmlDiff.getDiffs();
					}
					else if (fFirstXml.exists()) {
						deleted.add(nextFilePath);
					}
					else if (fSecondXml.exists()) {
						added.add(firstFilePath);
					}
					else {
						assert false; // it is impossible that neither entries exist 
					}
				}
				catch (Exception err) {
				      err.printStackTrace();
				      System.exit(1);
				}
				
			}
		}
	} // findDiffs
	
	public static void delete(File file) throws IOException {
		if(file.isDirectory()){
			if(file.list().length==0) {
			   file.delete();
			}
			else {
	    	   String files[] = file.list();
	
	    	   for (String temp : files) {
	    	      File fileDelete = new File(file, temp);
	    	      delete(fileDelete);
	    	   }
	
	    	   if(file.list().length==0) {
	       	     file.delete();
	    	   }
			}
		}
		else {
			file.delete();
		}
	}
} // RevisionDiff

/* vim :set ts=4 tw=4 tws=4 */
